+++
title = "S6.2 Symbol fields and metadata filled out as required"
+++

. *Reference* field is selected appropriately for the symbol and is _visible_
. *Value* field contains the name of the symbol and is _visible_
. *Footprint* field contains footprint link for fully specified symbols and must be _invisible_
.. The footprint field must be empty for generic symbols
. *Datasheet* entry is filled out, and is _invisible_
. The symbol contains no other custom fields

Additional documentation is provided via two other fields:
[start=6]
. **Description** field contains comma-separated device information
.. For symbols with a default footprint, the simplified footprint name should be appended to the description e.g. `SOIC-8`
.. Part name should not be duplicated in the description field
. **Keywords** field contains space-separated keyword values. These values are used to assist in part searching and should not include filler words

{{< klcimg src="symbol_metadata" title="Symbol metadata example" >}}

